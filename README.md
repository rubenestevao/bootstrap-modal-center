# Bootstrap Modal Center

Adds vertical centered modal behavior to bootstrap.

Installing with bower:

Run `bower i --save bootstrap-modal-center=git@bitbucket.org:rubenestevao/bootstrap-modal-center.git`
in your terminal

